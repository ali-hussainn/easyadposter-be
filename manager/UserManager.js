const User = require('../models').User;
const Util = require('../utils/Util');
const db = require('../models');
const SqlQuery = require('../constants/SqlQuery');

module.exports = {
  getUsersForPaymentCron: function () {
    return new Promise((resolve, reject) => {
      db.sequelize
        .query(SqlQuery.CRON_FOR_PAYMENT, {
          type: db.sequelize.QueryTypes.SELECT,
        })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getAll: function () {
    return new Promise((resolve, reject) => {
      cars = db.sequelize
        .query(
          '' +
            'SELECT posts.id,\n' +
            ' posts.user_id,\n' +
            ' letgo.email,\n' +
            ' letgo.password,\n' +
            ' posts.image_path,\n' +
            ' posts.title,\n' +
            ' posts.body,\n' +
            ' posts.price\n' +
            " FROM Posts posts inner join Users u on u.id = posts.user_id and posts.isActive='1' \n" +
            "inner join LetgoUsers letgo on letgo.user_id = u.id and letgo.isActive='1'\n" +
            'where u.isActive=1\n' +
            ';',
          {
            type: db.sequelize.QueryTypes.SELECT,
          }
        )
        .then(resolve)
        .catch(reject);
      /* User.findAll({
        where: {
          isActive: '1'
        }
      }).then(users => {
        resolve(users)
      })
      .catch(reject)*/
    });
  },
  getAllUsers: function () {
    return new Promise((resolve, reject) => {
      cars = db.sequelize
        .query(
          `SELECT u.id, u.name, u.email, u.isActive, u.createdAt, u.updatedAt, c.id as c_id, c.email as c_email,
          c.isLoginFailed as c_isLoginFailed, c.isActive as c_isActive, c.createdAt as c_createdAt,
          c.updatedAt as c_updatedAt, c.start_posting as c_start_posting, l.id as l_id, l.email as l_email,
          l.isLoginFailed as l_isLoginFailed, l.isActive as l_isActive, l.createdAt as l_createdAt,
          l.updatedAt as l_updatedAt, l.start_posting as l_start_posting, o.id as o_id, o.email as o_email,
          o.isLoginFailed as o_isLoginFailed, o.isActive as o_isActive, o.createdAt as o_createdAt,
          o.updatedAt as o_updatedAt, o.start_posting as o_start_posting
          FROM Users u left join CraiglistUsers c on u.id = c.user_id AND c.isActive=1
          left join LetgoUsers l on u.id = l.user_id AND l.isActive=1
          left join OfferupUsers o on u.id = o.user_id AND o.isActive=1;`,
          {
            type: db.sequelize.QueryTypes.SELECT,
          }
        )
        .then(resolve)
        .catch(reject);
    });
  },

  getUserByEmailPassword: function (email, password) {
    return new Promise((resolve, reject) => {
      User.findAll({
        where: {
          email: email,
          password: password,
        },
      })
        .then((users) => {
          if (Util.isEmptyObject(users)) {
            reject(new Error('Invalid username or password.'));
          } else {
            resolve(users[0]);
          }
        })
        .catch(reject);
    });
  },
  getUserByEmail: function (email) {
    return new Promise((resolve) => {
      User.findAll({
        where: {
          email: email,
        },
      }).then((users) => {
        resolve(users);
      });
    });
  },

  saveUser: function (user) {
    return new Promise((resolve, reject) => {
      User.create(user).then(resolve).catch(reject);
    });
  },

  getById: function (id) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error('id is required');
      }
      User.findAll({
        where: {
          id: id,
        },
      })
        .then((users) => {
          resolve(users[0]);
        })
        .catch(reject);
    });
  },

  getEmailByIds: function (ids, attr) {
    return new Promise((resolve, reject) => {
      User.findAll({
        where: {
          id: ids,
        },
        attributes: attr,
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },

  update: function (user) {
    return new Promise((resolve, reject) => {
      User.update(user, {
        where: {
          id: user.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },
};
