const Post = require("../models").Post;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      Post.findAndCountAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },

  getPostDueOfferup: function (userId) {
    return new Promise((resolve, reject) => {
      Post.findAll({
        where: {
          user_id: userId,
          isActive: 1,
          isOfferup: 1,
          isOfferupHold: 0,
          lastPostedOfferup: {
            [Op.or]: {
              [Op.eq]: null,
              [Op.lt]: moment().subtract(2, "hours").toDate(),
            },
          },
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  getPostDueLetgo: function (userId) {
    return new Promise((resolve, reject) => {
      Post.findAll({
        where: {
          user_id: userId,
          isActive: 1,
          isLetgo: 1,
          isLetgoHold: 0,
          lastPostedLetgo: {
            [Op.or]: {
              [Op.eq]: null,
              [Op.lt]: moment().subtract(2, "hours").toDate(),
            },
          },
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  getPostDueCraiglist: function (userId) {
    return new Promise((resolve, reject) => {
      Post.findAll({
        where: {
          user_id: userId,
          isActive: 1,
          isCraiglist: 1,
          isCraiglistHold: 0,
          lastPostedCraiglist: {
            [Op.or]: {
              [Op.eq]: null,
              [Op.lt]: moment().subtract(2, "hours").toDate(),
            },
          },
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      Post.findAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  save: function (post) {
    return new Promise((resolve, reject) => {
      Post.create(post).then(resolve).catch(reject);
    });
  },
  update: function (post) {
    return new Promise((resolve, reject) => {
      Post.update(post, {
        where: {
          id: post.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },
  delete: function (id) {
    return new Promise((resolve, reject) => {
      Post.update({ isActive: 0 }, { returning: true, where: { id: id } })
        .then(resolve)
        .catch(reject);
    });
  },
  getById: function (id) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      Post.findAll({
        where: {
          id: id,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      Post.findAll({
        where: {
          id: id,
          user_id: userId,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
};
