const TransactionLog = require("../models").TransactionLog;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");

module.exports = {
  getAll: function () {
    return new Promise((resolve, reject) => {
      TransactionLog.findAll()
        .then((transactionLogs) => {
          resolve(transactionLogs);
        })
        .catch(reject);
    });
  },
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      TransactionLog.findAndCountAll({
        where: {
          user_id: userId,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      TransactionLog.findAll({
        where: {
          user_id: userId,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  save: function (transactionLog) {
    return new Promise((resolve, reject) => {
      TransactionLog.create(transactionLog).then(resolve).catch(reject);
    });
  },
  update: function (transactionLog) {
    return new Promise((resolve, reject) => {
      TransactionLog.update(transactionLog, {
        where: {
          id: transactionLog.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },
};
