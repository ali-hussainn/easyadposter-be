const CraiglistUser = require("../models").CraiglistUser;
const db = require("../models");
const SqlQuery = require("../constants/SqlQuery");

module.exports = {
  getAll: function () {
    return new Promise((resolve, reject) => {
      CraiglistUser.findAll({
        where: {
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getAllForBot: function () {
    return new Promise((resolve, reject) => {
      db.sequelize
        .query(SqlQuery.BOT_SELECT_CRAIGLIST_USER, {
          type: db.sequelize.QueryTypes.SELECT,
        })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      CraiglistUser.findAndCountAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },

  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      CraiglistUser.findAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  save: function (user) {
    return new Promise((resolve, reject) => {
      CraiglistUser.create(user).then(resolve).catch(reject);
    });
  },
  update: function (user) {
    return new Promise((resolve, reject) => {
      CraiglistUser.update(user, {
        where: {
          id: user.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },

  delete: function (id) {
    return new Promise((resolve, reject) => {
      CraiglistUser.update(
        { isActive: 0 },
        { returning: true, where: { id: id } }
      )
        .then(resolve)
        .catch(reject);
    });
  },
  getByEmail: function (email) {
    return new Promise((resolve, reject) => {
      CraiglistUser.findAll({
        where: {
          email: email,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getById: function (id) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      CraiglistUser.findAll({
        where: {
          id: id,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      CraiglistUser.findAll({
        where: {
          id: id,
          user_id: userId,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
};
