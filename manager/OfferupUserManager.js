const OfferupUser = require("../models").OfferupUser;
const db = require("../models");
const SqlQuery = require("../constants/SqlQuery");

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      OfferupUser.findAndCountAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getAll: function () {
    return new Promise((resolve, reject) => {
      OfferupUser.findAll({
        where: {
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getAllForBot: function () {
    return new Promise((resolve, reject) => {
      db.sequelize
        .query(SqlQuery.BOT_SELECT_OFFERUP_USER, {
          type: db.sequelize.QueryTypes.SELECT,
        })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      OfferupUser.findAll({
        where: {
          user_id: userId,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  save: function (user) {
    return new Promise((resolve, reject) => {
      OfferupUser.create(user).then(resolve).catch(reject);
    });
  },
  update: function (user) {
    return new Promise((resolve, reject) => {
      OfferupUser.update(user, {
        where: {
          id: user.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },

  delete: function (id) {
    return new Promise((resolve, reject) => {
      OfferupUser.update(
        { isActive: 0 },
        { returning: true, where: { id: id } }
      )
        .then(resolve)
        .catch(reject);
    });
  },
  getByEmail: function (email) {
    return new Promise((resolve, reject) => {
      OfferupUser.findAll({
        where: {
          email: email,
          isActive: 1,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getById: function (id) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      OfferupUser.findAll({
        where: {
          id: id,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (id === undefined) {
        throw new Error("id is required");
      }
      OfferupUser.findAll({
        where: {
          id: id,
          user_id: userId,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
};
