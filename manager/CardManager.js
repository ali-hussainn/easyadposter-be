const Card = require("../models").Card;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      Card.findAndCountAll({
        where: {
          user_id: userId,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      Card.findAll({
        where: {
          user_id: userId,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  getByCardId: function (card_id) {
    return new Promise((resolve, reject) => {
      Card.findAll({
        where: {
          card_id: card_id,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  save: function (card) {
    return new Promise((resolve, reject) => {
      Card.create(card).then(resolve).catch(reject);
    });
  },
  update: function (card) {
    return new Promise((resolve, reject) => {
      Card.update(card, {
        where: {
          id: card.id,
        },
      })
        .then(resolve)
        .catch(reject);
    });
  },
};
