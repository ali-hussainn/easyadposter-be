const PostLog = require('../models').PostLog;

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      PostLog.findAndCountAll({
        where: {
          user_id: userId,
        },
      })
        .then((users) => {
          resolve(users);
        })
        .catch(reject);
    });
  },

  getLogsForAdmin: function (limit, offset) {
    return new Promise((resolve, reject) => {
      PostLog.findAndCountAll({
        order: [['createdAt', 'DESC']],
        limit,
        offset,
      })
        .then((logs) => {
          resolve(logs);
        })
        .catch(reject);
    });
  },

  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      PostLog.findAll({
        where: {
          user_id: userId,
        },
      })
        .then((posts) => {
          resolve(posts);
        })
        .catch(reject);
    });
  },
  save: function (post) {
    delete post.createdAt;
    delete post.updatedAt;
    return new Promise((resolve, reject) => {
      PostLog.create(post).then(resolve).catch(reject);
    });
  },
};
