var createError = require("http-errors");
var express = require("express");
var cors = require("cors");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

//var indexRouter = require('./routes/index');
var usersRouter = require("./routes/users");
var letgoUserRouter = require("./routes/letgouser");
var craiglistUserRouter = require("./routes/craiglistuser");
var postRouter = require("./routes/post");
var filesRouter = require("./routes/file_uploader");
var postlogRouter = require("./routes/postlog");
var offerupUserRouter = require("./routes/offerupuser");
var cardRouter = require("./routes/card");
var transactionLogRouter = require("./routes/transactionLog");
var schedulerService = require("./service/SchedulerService");

var app = express();
app.use(cors());
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/users", usersRouter);
app.use("/letgoUsers", letgoUserRouter);
app.use("/craiglistUsers", craiglistUserRouter);
app.use("/offerupUsers", offerupUserRouter);
app.use("/posts", postRouter);
app.use("/postLogs", postlogRouter);
app.use("/files", filesRouter);
app.use("/cards", cardRouter);
app.use("/transactionLogs", transactionLogRouter);

app.use("/users/letgo", letgoUserRouter);
app.use("/users/craiglist", craiglistUserRouter);
app.use("/users/offerup", offerupUserRouter);
app.use("/users/posts", postRouter);
app.use("/posts/logs", postlogRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

schedulerService.initiate();
module.exports = app;
