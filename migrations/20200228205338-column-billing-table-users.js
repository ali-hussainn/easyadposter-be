"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn("Users", "billing_customer_id", {
        allowNull: true,
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("Users", "billing_card_id", {
        allowNull: true,
        type: Sequelize.STRING,
      }),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("Users", "billing_customer_id"),
      queryInterface.removeColumn("Users", "billing_card_id"),
    ]);
  },
};
