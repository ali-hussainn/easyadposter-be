"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Posts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      title: {
        type: Sequelize.STRING,
      },
      price: {
        type: Sequelize.STRING,
      },
      body: {
        type: Sequelize.STRING,
      },
      postalCode: {
        type: Sequelize.STRING,
      },
      postingCity: {
        type: Sequelize.STRING,
      },
      category: {
        type: Sequelize.STRING,
      },
      offerupCategory: {
        type: Sequelize.STRING,
      },
      image_path: {
        type: Sequelize.STRING,
      },
      isLetgoHold: {
        type: Sequelize.TINYINT,
        defaultValue: Sequelize.literal(0),
      },
      isOfferupHold: {
        type: Sequelize.TINYINT,
        defaultValue: Sequelize.literal(0),
      },
      isCraiglistHold: {
        type: Sequelize.TINYINT,
        defaultValue: Sequelize.literal(0),
      },
      isActive: {
        type: Sequelize.TINYINT,
        defaultValue: Sequelize.literal(1),
      },
      isLetgo: {
        type: Sequelize.TINYINT,
        allowNull: false,
      },
      isOfferup: {
        type: Sequelize.TINYINT,
        allowNull: false,
      },
      isCraiglist: {
        allowNull: false,
        type: Sequelize.TINYINT,
      },
      lastPostedLetgo: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      lastPostedOfferup: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      lastPostedCraiglist: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("NULL ON UPDATE CURRENT_TIMESTAMP"),
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Posts");
  },
};
