"use strict";

module.exports = {
  up: function (queryInterface, Sequelize) {
    // logic for transforming into the new state
    return Promise.all([
      queryInterface.addColumn("CraiglistUsers", "start_posting", {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: Sequelize.literal(0),
      }),
      queryInterface.addColumn("LetgoUsers", "start_posting", {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: Sequelize.literal(0),
      }),
      queryInterface.addColumn("OfferupUsers", "start_posting", {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: Sequelize.literal(0),
      }),
    ]);
  },

  down: function (queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn("CraiglistUsers", "start_posting"),

      queryInterface.removeColumn("LetgoUsers", "start_posting"),

      queryInterface.removeColumn("OfferupUsers", "start_posting"),
    ]);
  },
};
