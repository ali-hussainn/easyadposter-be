"use strict";

module.exports = {
  up: function (queryInterface, Sequelize) {
    // logic for transforming into the new state
    return Promise.all([
      queryInterface.addColumn("Posts", "mobile_os", {
        allowNull: true,
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("PostLogs", "mobile_os", {
        allowNull: true,
        type: Sequelize.STRING,
      }),
    ]);
  },

  down: function (queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn("Posts", "mobile_os"),

      queryInterface.removeColumn("PostLogs", "mobile_os"),
    ]);
  },
};
