var bcrypt = require("bcryptjs");
var Constant = require("../constants/Constant");
var jwt = require("jsonwebtoken");

module.exports = {
  addTotalCountHeader: function (res, result) {
    res.set("Access-Control-Expose-Headers", "X-Total-Count");
    res.set("X-Total-Count", result.count);
  },
  isEmptyObject: function (obj) {
    return !Object.keys(obj).length;
  },

  encrypt: function (string) {
    return bcrypt.hashSync(string, 8);
  },

  compareEncrypted: function (simple, encrypted) {
    return bcrypt.compareSync(simple, encrypted);
  },

  createToken: function (userId) {
    return jwt.sign({ id: userId }, Constant.SECRET_KEY, {
      expiresIn: 86400, // expires in 24 hours
    });
  },

  createTokenAdmin: function (userId) {
    return jwt.sign({ id: userId }, Constant.INITIAL_VECTOR, {
      expiresIn: 86400, // expires in 24 hours
    });
  },

  cloneJson: function (src) {
    return JSON.parse(JSON.stringify(src));
  },
};
