var aes256 = require('aes256');
var Constant = require('../constants/Constant');

const key = Constant.ENCRYPTOR_KEY;

function encryptPass(data) {
  return aes256.encrypt(key, data);
}

function decryptPass(data) {
  return aes256.decrypt(key, data);
}

module.exports = {
  encryptPass: encryptPass,
  decryptPass: decryptPass,
};
