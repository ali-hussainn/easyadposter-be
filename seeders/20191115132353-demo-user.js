"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          name: "Faraz",
          email: "faraz@email.com",
          password:
            "$2a$08$FukbmysabEG7PuE70yI.QOLGUdVC13ZDw439B4MgLj0z7qRxydlF6",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
