"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Posts",
      [
        {
          title: "My Mobile",
          price: "10",
          user_id: 1,
          body: "body",
          image_path: "./1.jpg",
          postalCode: "32789",
          postingCity: "orl",
          category: "153",
          mobile_os: "#ui-id-5",
          offerupCategory: "Cell Phones",
          isLetgo: "1",
          isCraiglist: "1",
          isOfferup: "1",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Posts", null, {});
  },
};
