var express = require("express");
var router = express.Router();
var userService = require("../service/UserService");
var cardService = require("../service/CardService");
var Util = require("../utils/Util");
var RequestFilterService = require("../service/RequestFilterService");

/* GET users listing. */

router.get("/", RequestFilterService.validateRequestAdmin, function (
  req,
  res,
  next
) {
  userService
    .getAll()
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500);
      console.log(error);
      res.json({ error: error.message });
    });
});

router.get("/all", RequestFilterService.validateRequestAdminWeb, function (
  req,
  res,
  next
) {
  userService
    .getAllUsers()
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500);
      console.log(error);
      res.json({ error: error.message });
    });
});

router.get("/:userId", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  userService
    .getById(req.params.userId)
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.post("/cron", RequestFilterService.validateRequestAdmin, function (
  req,
  res,
  next
) {
  userService
    .getUsersForPaymentCron()
    .then((users) => {
      console.log(users);
      users.forEach((user, i) => {
        cardService.chargeCard(user);
      });
      res.json({ status: "success" });
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.post("/", function (req, res, next) {
  delete req.body.billing_customer_id;
  delete req.body.billing_card_id;
  userService
    .signup(req.body)
    .then((result) => res.send())
    .catch((error) => {
      res.status(400);
      res.json({ error: error.message });
    });
});

router.put("/", function (req, res, next) {
  delete req.body.billing_customer_id;
  delete req.body.billing_card_id;
  userService
    .signin(req.body.email, req.body.password)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send();
    });
});

router.put("/resetPassword", function (req, res, next) {
  userService
    .resetPassword(req.body.email)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send();
    });
});

router.put("/resetPasswordUpdate", function (req, res, next) {
  userService
    .resetPasswordUpdate(req.body.token, req.body.new_password)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      console.log(error);
      res.status(400);
      res.send({ error: error });
    });
});

router.put("/password", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  userService
    .updatePassword(
      req.params.userId,
      req.body.old_password,
      req.body.new_password
    )
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send();
    });
});

router.put("/admin", function (req, res, next) {
  userService
    .signinAdmin(req.body.email, req.body.password)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send();
    });
});

module.exports = router;
