var express = require('express');
var router = express.Router();
var PostLogService = require('../service/PostLogService');
var PostService = require('../service/PostService');
var RequestFilterService = require('../service/RequestFilterService');
var Util = require('../utils/Util');

router.get('/admin', RequestFilterService.validateRequestAdminWeb, function (
  req,
  res,
  next
) {
  PostLogService.getLogsForAdmin(+req.query.limit, +req.query.offset)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.get('/:id', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostLogService.getByUserId(req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

/* GET users listing. */
router.get('/', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostLogService.findByUserIdAndCountAll(req.params.userId)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.post('/:type', RequestFilterService.validateRequestAdmin, function (
  req,
  res,
  next
) {
  let type = req.params.type;
  if (type === 'letgo') {
    PostLogService.save(req.body)
      .then((result) => {
        let post = {
          id: result.post_id,
          lastPostedLetgo: new Date(),
        };
        PostService.update(post);
        res.json(result);
      })
      .catch((error) => {
        res.status(400);
        res.json({ error: error.message });
      });
  } else if (type === 'craiglist') {
    PostLogService.save(req.body)
      .then((result) => {
        let post = {
          id: result.post_id,
          lastPostedCraiglist: new Date(),
        };
        PostService.update(post);
        res.json(result);
      })
      .catch((error) => {
        res.status(400);
        res.json({ error: error.message });
      });
  } else if (type === 'offerup') {
    PostLogService.save(req.body)
      .then((result) => {
        let post = {
          id: result.post_id,
          lastPostedOfferup: new Date(),
        };
        PostService.update(post);
        res.json(result);
      })
      .catch((error) => {
        res.status(400);
        res.json({ error: error.message });
      });
  } else {
    res.status(400);
    res.json({ error: 'invalid' });
  }
});

module.exports = router;
