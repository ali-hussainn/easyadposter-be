var express = require("express");
var router = express.Router();
var PostService = require("../service/PostService");
var RequestFilterService = require("../service/RequestFilterService");
var Util = require("../utils/Util");

/* GET users listing. */
router.get("/", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostService.findByUserIdAndCountAll(req.params.userId)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.post("/", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostService.save(req.body)
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(400);
      res.json({ error: error.message });
    });
});

router.put("/:id", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostService.update(req.body, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.get("/:id", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostService.getByIdAndUserId(req.params.id, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.delete("/:id", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  PostService.delete(req.params.id, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});
module.exports = router;
