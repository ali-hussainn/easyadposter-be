var express = require('express');
var router = express.Router();
var letgoUserService = require('../service/LetgoUserService');
var RequestFilterService = require('../service/RequestFilterService');
var Util = require('../utils/Util');
const Encryptor = require('../utils/Encryptor');

/* GET users listing. */
router.get('/admin', RequestFilterService.validateRequestAdminWeb, function (
  req,
  res,
  next
) {
  letgoUserService
    .getAll()
    .then((result) => res.json(result))
    .catch((error) => {
      console.error(error);
      res.status(500);
      res.json({ error: error.message });
    });
});

router.get('/', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  letgoUserService
    .findByUserIdAndCountAll(req.params.userId)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

router.post('/', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  letgoUserService
    .save(req.body)
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(400);
      res.json({ error: error.message });
    });
});

router.put('/admin', RequestFilterService.validateRequestAdminWeb, function (
  req,
  res,
  next
) {
  letgoUserService
    .update(req.body, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.put('/', RequestFilterService.validateRequestAdminOrUser, function (
  req,
  res,
  next
) {
  delete req.body.start_posting;
  letgoUserService
    .update(req.body, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.put('/:id', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  delete req.body.start_posting;
  delete req.body.createdAt;
  delete req.body.updatedAt;
  delete req.body.isActive;
  if (req.body.password) {
    req.body.password = Encryptor.encryptPass(req.body.password);
  }
  letgoUserService
    .update(req.body, req.params.userId, true)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.get('/:id', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  letgoUserService
    .getByIdAndUserId(req.params.id, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

router.delete(
  '/admin/:id',
  RequestFilterService.validateRequestAdminWeb,
  function (req, res, next) {
    delete req.body.user_id;
    letgoUserService
      .delete(req.params.id)
      .then((result) => {
        res.json(result);
      })
      .catch((error) => {
        res.status(400);
        res.send({ error: error.message });
      });
  }
);

router.delete('/:id', RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  letgoUserService
    .delete(req.params.id, req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(400);
      res.send({ error: error.message });
    });
});

module.exports = router;
