var express = require("express");
var router = express.Router();
var RequestFilterService = require("../service/RequestFilterService");
var TransactionLogService = require("../service/TransactionLogService");
var Util = require("../utils/Util");

router.get("/admin", RequestFilterService.validateRequestAdminWeb, function (
  req,
  res,
  next
) {
  TransactionLogService.getAll()
    .then((result) => res.json(result))
    .catch((error) => {
      console.error(error);
      res.status(500);
      res.json({ error: error.message });
    });
});

router.get("/", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  TransactionLogService.findByUserIdAndCountAll(req.params.userId)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

module.exports = router;
