var express = require("express");
var router = express.Router();
var RequestFilterService = require("../service/RequestFilterService");
var CardService = require("../service/CardService");
var Util = require("../utils/Util");

router.post("/", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  CardService.addCard(req.params.userId, req.body)
    .then((result) => res.json({ result: "success" }))
    .catch((error) => {
      res.status(500);
      console.error(error);
      res.send("Failed to add Card");
    });
});

router.get("/", RequestFilterService.validateRequest, function (
  req,
  res,
  next
) {
  CardService.findByUserIdAndCountAll(req.params.userId)
    .then((result) => {
      Util.addTotalCountHeader(res, result);
      res.json(result.rows);
    })
    .catch((error) => {
      res.status(500);
      res.json({ error: error.message });
    });
});

module.exports = router;
