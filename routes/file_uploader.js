var express = require("express");
var router = express.Router();
var multer = require("multer");
const mkdirp = require("mkdirp");
var RequestFilterService = require("../service/RequestFilterService");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    let uploadPath = "public/files/" + req.params.type + "/";
    mkdirp.sync(uploadPath);
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    var filetype = "";
    if (file.mimetype === "image/gif") {
      filetype = "gif";
    }
    if (file.mimetype === "image/png") {
      filetype = "png";
    }
    if (file.mimetype === "image/jpeg") {
      filetype = "jpg";
    }
    cb(null, req.params.id + "-" + Date.now() + "." + filetype);
  },
});
var upload = multer({ storage: storage });

router.post(
  "/upload/:type/:id",
  RequestFilterService.validateRequest,
  upload.single("file"),
  function (req, res, next) {
    if (!req.file) {
      res.status(500);
      res.json({ error: "Upload Failed." });
    } else {
      res.json({ fileUrl: req.file.filename });
    }
  }
);

module.exports = router;
