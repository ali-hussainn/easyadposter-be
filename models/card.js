"use strict";
module.exports = (sequelize, DataTypes) => {
  const Card = sequelize.define(
    "Card",
    {
      card_id: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      billing_customer_id: DataTypes.STRING,
      card_brand: DataTypes.STRING,
      last_4: DataTypes.STRING,
      exp_month: DataTypes.STRING,
      exp_year: DataTypes.STRING,
    },
    {}
  );
  Card.associate = function (models) {
    // associations can be defined here
  };
  return Card;
};
