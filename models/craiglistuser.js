"use strict";
module.exports = (sequelize, DataTypes) => {
  const CraiglistUser = sequelize.define(
    "CraiglistUser",
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      isLoginFailed: DataTypes.TINYINT,
      start_posting: DataTypes.TINYINT,
      isActive: DataTypes.TINYINT,
    },
    {}
  );
  CraiglistUser.associate = function (models) {
    // associations can be defined here
  };
  CraiglistUser.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());
    delete values.password;
    return values;
  };

  return CraiglistUser;
};
