"use strict";
module.exports = (sequelize, DataTypes) => {
  const LetgoUser = sequelize.define(
    "LetgoUser",
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      isActive: DataTypes.TINYINT,
      start_posting: DataTypes.TINYINT,
      isLoginFailed: DataTypes.TINYINT,
    },
    {}
  );
  LetgoUser.associate = function (models) {
    // associations can be defined here
  };
  LetgoUser.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());
    delete values.password;
    return values;
  };
  return LetgoUser;
};
