"use strict";
module.exports = (sequelize, DataTypes) => {
  const PostLog = sequelize.define(
    "PostLog",
    {
      user_id: DataTypes.INTEGER,
      post_id: DataTypes.INTEGER,
      title: DataTypes.STRING,
      price: DataTypes.STRING,
      body: DataTypes.STRING,
      postalCode: DataTypes.STRING,
      postingCity: DataTypes.STRING,
      category: DataTypes.STRING,
      offerupCategory: DataTypes.STRING,
      type: DataTypes.STRING,
      image_path: DataTypes.STRING,
      isSuccess: DataTypes.TINYINT,
      mobile_os: DataTypes.STRING,
    },
    {}
  );
  PostLog.associate = function (models) {
    // associations can be defined here
  };
  return PostLog;
};
