"use strict";
module.exports = (sequelize, DataTypes) => {
  const TransactionLog = sequelize.define(
    "TransactionLog",
    {
      payment_id: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      status: DataTypes.STRING,
      billing_customer_id: DataTypes.STRING,
      billing_card_id: DataTypes.STRING,
      amount: DataTypes.DOUBLE,
      failure_reason: DataTypes.STRING,
    },
    {}
  );
  TransactionLog.associate = function (models) {
    // associations can be defined here
  };
  return TransactionLog;
};
