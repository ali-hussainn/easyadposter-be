"use strict";
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define(
    "Post",
    {
      user_id: DataTypes.INTEGER,
      title: DataTypes.STRING,
      price: DataTypes.STRING,
      body: DataTypes.STRING,
      postalCode: DataTypes.STRING,
      postingCity: DataTypes.STRING,
      category: DataTypes.STRING,
      offerupCategory: DataTypes.STRING,
      image_path: DataTypes.STRING,
      lastPostedLetgo: DataTypes.DATE,
      lastPostedCraiglist: DataTypes.DATE,
      lastPostedOfferup: DataTypes.DATE,
      isLetgo: DataTypes.TINYINT,
      isLetgoHold: DataTypes.TINYINT,
      isCraiglist: DataTypes.TINYINT,
      isCraiglistHold: DataTypes.TINYINT,
      isOfferup: DataTypes.TINYINT,
      isOfferupHold: DataTypes.TINYINT,
      isActive: DataTypes.TINYINT,
      mobile_os: DataTypes.STRING,
    },
    {}
  );
  Post.associate = function (models) {
    // associations can be defined here
  };
  return Post;
};
