"use strict";
module.exports = (sequelize, DataTypes) => {
  const OfferupUser = sequelize.define(
    "OfferupUser",
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      isLoginFailed: DataTypes.TINYINT,
      start_posting: DataTypes.TINYINT,
      isActive: DataTypes.TINYINT,
    },
    {}
  );
  OfferupUser.associate = function (models) {
    // associations can be defined here
  };
  OfferupUser.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());
    delete values.password;
    return values;
  };
  return OfferupUser;
};
