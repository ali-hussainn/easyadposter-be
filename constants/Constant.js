module.exports = {
  SECRET_KEY: process.env.SECRET_KEY || '12333',
  ADMIN_TOKEN: process.env.ADMIN_TOKEN || 'XYZ',
  INITIAL_VECTOR: process.env.INITIAL_VECTOR || '1234567890123456',
  ENCRYPTOR_KEY:
    process.env.ENCRYPTOR_KEY || '12345678901234567890123456789012',
  SQUARE_AUTH_TOKEN:
    process.env.SQUARE_AUTH_TOKEN ||
    'EAAAEIseNS9cdUTw9LQw7blrLYENbWtBw4vkuRMCQI8CHkUvy66mBOydJ9tBvug4',
  SQUARE_API_ENDPOINT:
    process.env.SQUARE_API_ENDPOINT || 'https://connect.squareupsandbox.com',
};
