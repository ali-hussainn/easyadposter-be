module.exports = {
  BOT_SELECT_LETGO_USER:
    "SELECT * FROM LetgoUsers WHERE start_posting=1 and id in (" +
    "select min(id) from LetgoUsers where isActive=1 and isLoginFailed=0 " +
    "group by user_id)",
  BOT_SELECT_CRAIGLIST_USER:
    "SELECT * FROM CraiglistUsers WHERE start_posting=1 and id in (" +
    "select min(id) from CraiglistUsers where isActive=1 and isLoginFailed=0 " +
    "group by user_id)",
  BOT_SELECT_OFFERUP_USER:
    "SELECT * FROM OfferupUsers WHERE start_posting=1 and id in (" +
    "select min(id) from OfferupUsers where isActive=1 and isLoginFailed=0 " +
    "group by user_id)",
  CRON_FOR_PAYMENT:
    "SELECT * from Users where billing_card_id is not null and id not in (" +
    "SELECT user_id from TransactionLogs where status='success' and createdAt > date_sub(now(), interval 1 month))",
};
