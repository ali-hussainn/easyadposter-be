const Util = require('../utils/Util');
const LetgoUserManager = require('../manager/LetgoUserManager');
const postManager = require('../manager/PostManager');
const Encryptor = require('../utils/Encryptor');

module.exports = {
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      LetgoUserManager.getByIdAndUserId(id, userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      LetgoUserManager.findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getAll: async function () {
    return new Promise((resolve, reject) => {
      LetgoUserManager.getAll()
        .then((users) => resolve(users))
        .catch(reject);
    });
  },
  getAllForBot: async function () {
    return new Promise((resolve, reject) => {
      LetgoUserManager.getAllForBot()
        .then(async function (letgoUsers) {
          var restData = [];
          for (let i = 0; i < letgoUsers.length; i++) {
            let letgoUser = letgoUsers[i];
            let posts = await postManager.getPostDueLetgo(letgoUser.user_id);
            if (posts.length > 0) {
              restData[i] = {
                letgoUser: letgoUser,
                posts: posts.length ? posts : [],
              };
            }
          }
          resolve(restData);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      LetgoUserManager.getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: function (user) {
    return new Promise((resolve, reject) => {
      LetgoUserManager.getByEmail(user.email).then((users) => {
        if (Util.isEmptyObject(users) === false || user.id !== undefined) {
          reject(new Error('Letgo User already exist'));
        } else {
          user.password = Encryptor.encryptPass(user.password);
          LetgoUserManager.save(user).then(resolve).catch(reject);
        }
      });
    });
  },

  update: function (user, userId, selfUpdate = false) {
    if (!selfUpdate) {
      delete user.password;
    }
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = LetgoUserManager.getById(user.id);
      } else {
        promise = LetgoUserManager.getByIdAndUserId(user.id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Letgo User not found'));
          } else {
            LetgoUserManager.getByEmail(user.email)
              .then((users) => {
                if (
                  users !== undefined &&
                  users !== null &&
                  users[0] !== undefined &&
                  users[0].id !== user.id
                ) {
                  reject(new Error('Letgo User already exist'));
                } else {
                  LetgoUserManager.update(user).then(resolve).catch(reject);
                }
              })
              .catch(reject);
          }
        })
        .catch(reject);
    });
  },
  delete: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = LetgoUserManager.getById(id);
      } else {
        promise = LetgoUserManager.getByIdAndUserId(id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Letgo User not found'));
          } else {
            LetgoUserManager.delete(id).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
};
