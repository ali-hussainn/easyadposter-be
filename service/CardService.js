var SquareConnect = require("square-connect");
var userService = require("../service/UserService");
var emailService = require("../service/EmailService");
var transactionLogService = require("../service/TransactionLogService");
var Constants = require("../constants/Constant");
var CardManager = require("../manager/CardManager");
var defaultClient = SquareConnect.ApiClient.instance;
const Util = require("../utils/Util");

// Set sandbox url
if (Constants.SQUARE_API_ENDPOINT != "prod") {
  defaultClient.basePath = "https://connect.squareupsandbox.com";
}
// Configure OAuth2 access token for authorization: oauth2
var oauth2 = defaultClient.authentications["oauth2"];
// Set sandbox access token
oauth2.accessToken = Constants.SQUARE_AUTH_TOKEN;
// Pass client to API
//var api = new SquareConnect.LocationsApi();

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      CardManager.findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  createCustomer: async function (user) {
    var apiInstance = new SquareConnect.CustomersApi();

    var body = new SquareConnect.CreateCustomerRequest(); // CreateCustomerRequest | An object containing the fields to POST for the request.  See the corresponding object definition for field details.
    body.given_name = user.name;
    body.email_address = user.email;
    body.reference_id = "cust_" + user.id;
    let data = await apiInstance.createCustomer(body);
    user.billing_customer_id = data.customer.id;
    let up_user = { id: user.id, billing_customer_id: data.customer.id };
    await userService.update(up_user);
    return up_user.billing_customer_id;
  },

  addCard: async function (userId, cardRequest) {
    let user = await userService.getById(userId);

    if (
      user.billing_customer_id === undefined ||
      user.billing_customer_id === null
    ) {
      user.billing_customer_id = await this.createCustomer(user);
    }

    var apiInstance = new SquareConnect.CustomersApi();

    var body = new SquareConnect.CreateCustomerCardRequest(); // CreateCustomerCardRequest | An object containing the fields to POST for the request.  See the corresponding object definition for field details.

    body.card_nonce = cardRequest.nonce;
    body.verification_token = cardRequest.token;

    let data = await apiInstance.createCustomerCard(
      user.billing_customer_id,
      body
    );
    let card = data.card;
    let up_user = { id: user.id, billing_card_id: card.id };
    await userService.update(up_user);
    card.card_id = card.id;
    card.billing_customer_id = user.billing_customer_id;
    card.user_id = user.id;
    delete card.id;
    CardManager.getByCardId(card.card_id).then((res) => {
      if (Util.isEmptyObject(res) === true) {
        CardManager.save(card);
      } else {
        card.id = res.id;
        CardManager.update(card);
      }
    });
  },

  chargeCard: async function (user) {
    let charge = 99.0;
    let transactionLog = {
      user_id: user.id,
      billing_card_id: user.billing_card_id,
      billing_customer_id: user.billing_customer_id,
      amount: charge,
    };
    let saved = await transactionLogService.save(transactionLog);
    transactionLog.id = saved.id;
    let money = {
      amount: charge * 100,
      currency: "USD",
    };
    var apiInstance = new SquareConnect.PaymentsApi();
    var body = new SquareConnect.CreatePaymentRequest(); // CreatePaymentRequest | An object containing the fields to POST for the request.  See the corresponding object definition for field details.
    body.source_id = transactionLog.billing_card_id;
    body.idempotency_key = "" + transactionLog.id;
    body.customer_id = transactionLog.billing_customer_id;
    body.amount_money = money;
    apiInstance.createPayment(body).then(
      function (data) {
        transactionLog.payment_id = data.payment.id;
        transactionLog.status = "success";
        transactionLogService.update(transactionLog);
      },
      function (error) {
        console.error(error);
        transactionLog.status = "failed";
        transactionLog.failure_reason = error.text;
        transactionLogService.update(transactionLog);
        emailService.failPayment(user);
      }
    );
  },
};
