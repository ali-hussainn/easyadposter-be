const cron = require("node-cron");
const userService = require("../service/UserService");
const cardService = require("../service/CardService");

module.exports = {
  initiate: function () {
    cron.schedule("* 2 * * *", function () {
      console.log("running a task every two hour");
      userService
        .getUsersForPaymentCron()
        .then((users) => {
          console.log(users);
          users.forEach((user, i) => {
            cardService.chargeCard(user);
          });
        })
        .catch((error) => {
          console.error(error);
        });
    });
  },
};
