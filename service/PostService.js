const Util = require("../utils/Util");
const PostManager = require("../manager/PostManager");

module.exports = {
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      PostManager.getByIdAndUserId(id, userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      PostManager.findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      PostManager.getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: function (post) {
    return new Promise((resolve, reject) => {
      PostManager.save(post).then(resolve).catch(reject);
    });
  },

  update: function (post, userId) {
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = PostManager.getById(post.id);
      } else {
        promise = PostManager.getByIdAndUserId(post.id, userId);
      }
      promise
        .then((p) => {
          if (Util.isEmptyObject(p) === true) {
            reject(new Error("Post not found"));
          } else {
            PostManager.update(post).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
  delete: function (id, userId) {
    return new Promise((resolve, reject) => {
      PostManager.getByIdAndUserId(id, userId)
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error("Post not found"));
          } else {
            PostManager.delete(id).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
};
