const Util = require("../utils/Util");
const transactionLogManager = require("../manager/TransactionLogManager");
module.exports = {
  getAll: async function () {
    return new Promise((resolve, reject) => {
      transactionLogManager
        .getAll()
        .then((transactionLogs) => resolve(transactionLogs))
        .catch(reject);
    });
  },
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      transactionLogManager
        .findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      transactionLogManager
        .getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: async function (transactionLog) {
    return await transactionLogManager.save(transactionLog);
  },

  update: async function (transactionLog) {
    return await transactionLogManager.update(transactionLog);
  },
};
