const Util = require('../utils/Util');
const craiglistUserManager = require('../manager/CraiglistUserManager');
const postManager = require('../manager/PostManager');
const Encryptor = require('../utils/Encryptor');
module.exports = {
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      craiglistUserManager
        .getByIdAndUserId(id, userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  getAll: async function () {
    return new Promise((resolve, reject) => {
      craiglistUserManager
        .getAll()
        .then((users) => resolve(users))
        .catch(reject);
    });
  },
  getAllForBot: async function () {
    return new Promise((resolve, reject) => {
      craiglistUserManager
        .getAllForBot()
        .then(async function (craiglistUsers) {
          var restData = [];
          for (let i = 0; i < craiglistUsers.length; i++) {
            let craiglistUser = craiglistUsers[i];
            let posts = await postManager.getPostDueCraiglist(
              craiglistUser.user_id
            );
            if (posts.length > 0) {
              restData[i] = {
                craiglistUser: craiglistUser,
                posts: posts,
              };
            }
          }
          resolve(restData);
        })
        .catch(reject);
    });
  },
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      craiglistUserManager
        .findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      craiglistUserManager
        .getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: function (user) {
    return new Promise((resolve, reject) => {
      craiglistUserManager.getByEmail(user.email).then((users) => {
        if (Util.isEmptyObject(users) === false || user.id !== undefined) {
          reject(new Error('Craiglist User already exist'));
        } else {
          user.password = Encryptor.encryptPass(user.password);
          craiglistUserManager.save(user).then(resolve).catch(reject);
        }
      });
    });
  },

  update: function (user, userId, selfUpdate) {
    if (!selfUpdate) {
      delete user.password;
    }
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = craiglistUserManager.getById(user.id);
      } else {
        promise = craiglistUserManager.getByIdAndUserId(user.id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Craiglist User not found'));
          } else {
            craiglistUserManager
              .getByEmail(user.email)
              .then((users) => {
                if (
                  users !== undefined &&
                  users !== null &&
                  users[0] !== undefined &&
                  users[0].id !== user.id
                ) {
                  reject(new Error('Craiglist User already exist'));
                } else {
                  craiglistUserManager.update(user).then(resolve).catch(reject);
                }
              })
              .catch(reject);
          }
        })
        .catch(reject);
    });
  },
  delete: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = craiglistUserManager.getById(id);
      } else {
        promise = craiglistUserManager.getByIdAndUserId(id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Craiglist User not found'));
          } else {
            craiglistUserManager.delete(id).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
};
