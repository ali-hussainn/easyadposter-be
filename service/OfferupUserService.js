const Util = require('../utils/Util');
const offerupUserManager = require('../manager/OfferupUserManager');
const postManager = require('../manager/PostManager');
const Encryptor = require('../utils/Encryptor');

module.exports = {
  getByIdAndUserId: function (id, userId) {
    return new Promise((resolve, reject) => {
      offerupUserManager
        .getByIdAndUserId(id, userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      offerupUserManager
        .findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getAll: async function () {
    return new Promise((resolve, reject) => {
      offerupUserManager
        .getAll()
        .then((users) => resolve(users))
        .catch(reject);
    });
  },
  getAllForBot: async function () {
    return new Promise((resolve, reject) => {
      offerupUserManager
        .getAllForBot()
        .then(async function (offerupUsers) {
          var restData = [{}];
          for (let i = 0; i < offerupUsers.length; i++) {
            let offerupUser = offerupUsers[i];
            let posts = await postManager.getPostDueOfferup(
              offerupUser.user_id
            );
            if (posts.length > 0) {
              restData[i] = {
                offerupUser: offerupUser,
                posts: posts,
              };
            }
          }
          resolve(restData);
        })
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      offerupUserManager
        .getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: function (user) {
    return new Promise((resolve, reject) => {
      offerupUserManager.getByEmail(user.email).then((users) => {
        if (Util.isEmptyObject(users) === false || user.id !== undefined) {
          reject(new Error('Offerup User already exist'));
        } else {
          user.password = Encryptor.encryptPass(user.password);
          offerupUserManager.save(user).then(resolve).catch(reject);
        }
      });
    });
  },

  update: function (user, userId, selfUpdate) {
    if (!selfUpdate) {
      delete user.password;
    }
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = offerupUserManager.getById(user.id);
      } else {
        promise = offerupUserManager.getByIdAndUserId(user.id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Offerup User not found'));
          } else {
            offerupUserManager
              .getByEmail(user.email)
              .then((users) => {
                if (
                  users !== undefined &&
                  users !== null &&
                  users[0] !== undefined &&
                  users[0].id !== user.id
                ) {
                  reject(new Error('Offerup User already exist'));
                } else {
                  offerupUserManager.update(user).then(resolve).catch(reject);
                }
              })
              .catch(reject);
          }
        })
        .catch(reject);
    });
  },
  delete: function (id, userId) {
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = offerupUserManager.getById(id);
      } else {
        promise = offerupUserManager.getByIdAndUserId(id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('Offerup User not found'));
          } else {
            offerupUserManager.delete(id).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
};
