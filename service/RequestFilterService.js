var Constant = require('../constants/Constant');
var jwt = require('jsonwebtoken');

module.exports = {
  validateRequest: validateRequest,
  validateRequestAdmin: validateRequestAdmin,
  validateRequestAdminWeb: validateRequestAdminWeb,
  verifyToken: verifyToken,
  validateRequestAdminOrUser: function (req, res, next) {
    var token = req.headers['x-access-token'];
    if (token) {
      validateRequest(req, res, next);
      return;
    } else {
      validateRequestAdmin(req, res, next);
      return;
    }
    next();
  },
};

function validateRequest(req, res, next) {
  console.log('heheheheheh');
  var token = req.headers['x-access-token'];
  if (!token) return res.status(403).send({ error: 'No token provided.' });

  jwt.verify(token, Constant.SECRET_KEY, function (err, decoded) {
    if (err)
      return res.status(500).send({ error: 'Failed to authenticate token.' });

    req.userId = decoded.id;
    req.params.userId = decoded.id;
    req.user_id = decoded.id;
    req.body.user_id = decoded.id;

    next();
  });
}

function verifyToken(token) {
  /*jwt.verify(token, Constant.SECRET_KEY, function(err, decoded) {
    if (err){return undefined;}
    return decoded.id;
  });
  */
  return jwt.verify(token, Constant.SECRET_KEY).id;
}

function validateRequestAdminWeb(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token) return res.status(403).send({ error: 'No token provided.' });

  jwt.verify(token, Constant.INITIAL_VECTOR, function (err, decoded) {
    if (err)
      return res.status(500).send({ error: 'Failed to authenticate token.' });

    req.userId = req.body.user_id;
    req.params.userId = req.body.user_id;
    req.user_id = req.body.user_id;

    next();
  });
}

function validateRequestAdmin(req, res, next) {
  var token = req.headers['x-access-token-2'];
  if (!token) {
    return res.status(403).send({ error: 'No token provided.' });
  }
  if (token !== Constant.ADMIN_TOKEN) {
    return res.status(500).send({ error: 'Failed to authenticate token.' });
  }
  req.params.userId = req.body.user_id;
  next();
}
