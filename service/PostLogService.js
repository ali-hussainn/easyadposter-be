const PostLogManager = require('../manager/PostLogManager');
const UserService = require('./UserService');

module.exports = {
  findByUserIdAndCountAll: function (userId) {
    return new Promise((resolve, reject) => {
      PostLogManager.findByUserIdAndCountAll(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },
  getByUserId: function (userId) {
    return new Promise((resolve, reject) => {
      PostLogManager.getByUserId(userId)
        .then((result) => resolve(result))
        .catch(reject);
    });
  },

  save: function (postLog) {
    return new Promise((resolve, reject) => {
      PostLogManager.save(postLog).then(resolve).catch(reject);
    });
  },

  getLogsForAdmin: function (limit, offset) {
    return new Promise((resolve, reject) => {
      PostLogManager.getLogsForAdmin(limit, offset)
        .then((resp) => {
          const logs = resp.rows;
          if (resp.rows.length) {
            const ids = logs.map((log) => log.user_id);
            UserService.getEmailByIds(ids, ['email', 'id'])
              .then((users) => {
                resolve({
                  logs,
                  users
                });
              })
              .catch(reject);
          } else {
            reject(new Error('No logs found'))
          }
        })
        .catch(reject);
    });
  },
};
