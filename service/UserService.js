const Util = require('../utils/Util');
const userManager = require('../manager/UserManager');
const craiglistUserService = require('../service/CraiglistUserService');
const letgoUserService = require('../service/LetgoUserService');
const offerupUserService = require('../service/OfferupUserService');
const requestFilterService = require('../service/RequestFilterService');
const emailService = require('../service/EmailService');
const encrypt = require('../utils/Encryptor');

module.exports = {
  getUsersForPaymentCron: async function () {
    users = await userManager.getUsersForPaymentCron();
    return users;
  },
  getAll: async function () {
    return new Promise((resolve, reject) => {
      letgoUserService
        .getAllForBot()
        .then((letgoUsers) => {
          craiglistUserService
            .getAllForBot()
            .then((craiglistUsers) => {
              offerupUserService
                .getAllForBot()
                .then((offerupUsers) => {
                  let restData = {
                    letgoUsers: letgoUsers,
                    craiglistUsers: craiglistUsers,
                    offerupUsers: offerupUsers,
                  };
                  resolve(restData);
                })
                .catch(reject);
            })
            .catch(reject);
        })
        .catch(reject);
    });
  },

  getAllUsers: function () {
    return new Promise((resolve, reject) => {
      userManager.getAllUsers().then(resolve).catch(reject);
    });
  },

  getEmailByIds: function (ids, attr) {
    return new Promise((resolve, reject) => {
      userManager.getEmailByIds(ids, attr).then(resolve).catch(reject);
    });
  },

  getById: function (id) {
    return new Promise((resolve, reject) => {
      userManager.getById(id).then(resolve).catch(reject);
    });
  },

  signup: function (user) {
    return new Promise((resolve, reject) => {
      userManager.getUserByEmail(user.email).then((users) => {
        if (Util.isEmptyObject(users) === false) {
          reject(new Error('User already exist'));
        }
        user.password = encrypt.encryptPass(user.password);
        userManager
          .saveUser(user)
          .then((res) => {
            emailService.signup(res);
            resolve(res);
          })
          .catch(reject);
      });
    });
  },

  signin: function (email, password) {
    return new Promise((resolve, reject) => {
      userManager
        .getUserByEmail(email)
        .then((users) => {
          if (Util.isEmptyObject(users)) {
            reject(new Error('Invalid login'));
            return;
          }
          let user = users[0];
          if (password !== encrypt.decryptPass(user.password)) {
            reject(new Error('Invalid login'));
          }
          user = user.toJSON();
          user.token = Util.createToken(user.id);
          resolve(user);
        })
        .catch((error) => {
          console.log('Login Error', error);
          reject(error);
        });
    });
  },

  resetPassword: function (email) {
    return new Promise((resolve, reject) => {
      userManager
        .getUserByEmail(email)
        .then((users) => {
          if (Util.isEmptyObject(users) === true) {
            reject(new Error('User not found'));
            return;
          }
          let user = users[0];
          user = user.toJSON();
          let token = Util.createToken(user.id);
          emailService.sendResetPassword(user, token);
          resolve({ status: 'Success' });
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  },

  resetPasswordUpdate: function (token, new_password) {
    return new Promise((resolve, reject) => {
      let user_id = requestFilterService.verifyToken(token);
      if (user_id === undefined) {
        reject(new Error('Invalid token'));
        return;
      }

      let uu = { id: user_id, password: encrypt.encryptPass(new_password) };
      userManager
        .update(uu)
        .then((res) => {
          resolve({ status: 'Success' });
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  },

  signinAdmin: function (email, password) {
    return new Promise((resolve, reject) => {
      userManager
        .getUserByEmail(email)
        .then((users) => {
          if (Util.isEmptyObject(users) !== false) {
            reject(new Error('Invalid login'));
          }
          let user = users[0];
          if (password !== encrypt.decryptPass(user.password)) {
            reject(new Error('Invalid login'));
          }

          if (email !== 'faraz@email.com') {
            reject(new Error('Invalid login'));
          }
          user = user.toJSON();
          user.token = Util.createTokenAdmin(user.id);
          resolve(user);
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  },

  update: function (user, userId) {
    console.log(user);
    delete user.password;
    return new Promise((resolve, reject) => {
      if (userId === undefined) {
        promise = userManager.getById(user.id);
      } else {
        promise = userManager.getByIdAndUserId(id, userId);
      }
      promise
        .then((u) => {
          if (Util.isEmptyObject(u) === true) {
            reject(new Error('User not found'));
          } else {
            userManager.update(user).then(resolve).catch(reject);
          }
        })
        .catch(reject);
    });
  },
  updatePassword: function (userId, old_password, new_password) {
    return new Promise((resolve, reject) => {
      userManager
        .getById(userId)
        .then((user) => {
          if (Util.isEmptyObject(user) !== false) {
            reject(new Error('Invalid User'));
          }
          if (old_password !== encrypt.decryptPass(user.password)) {
            reject(new Error('Invalid old password'));
          }
          user = user.toJSON();
          let user_up = {
            id: user.id,
            password: encrypt.encryptPass(new_password),
          };
          userManager.update(user_up).then(resolve).catch(reject);
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  },
};
